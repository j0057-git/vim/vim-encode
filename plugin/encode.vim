vnoremap <silent> <Plug>(encode#Encode)     :call encode#encode_sel()<CR>
vnoremap <silent> <Plug>(encode#Decode)     :call encode#decode_sel()<CR>
vnoremap <silent> <Plug>(encode#HashHex)    :call encode#hash_sel('hex')<CR>
vnoremap <silent> <Plug>(encode#HashBase64) :call encode#hash_sel('base64')<CR>
