function! encode#encode_sel() abort
    let encode = { 'base64':    "gvc\<C-R>=system('basenc --wrap=0 --base64', @\")\<CR>\<ESC>"
    \            , 'base64url': "gvc\<C-R>=system('basenc --wrap=0 --base64url', @\")\<CR>\<ESC>"
    \            , 'base32':    "gvc\<C-R>=system('basenc --wrap=0 --base32', @\")\<CR>\<ESC>"
    \            , 'base32hex': "gvc\<C-R>=system('basenc --wrap=0 --base32hex', @\")\<CR>\<ESC>"
    \            , 'base16':    "gvc\<C-R>=system('basenc --wrap=0 --base16', @\")\<CR>\<ESC>"
    \            , 'base2msbf': "gvc\<C-R>=system('basenc --wrap=0 --base2msbf', @\")\<CR>\<ESC>"
    \            , 'base2lsbf': "gvc\<C-R>=system('basenc --wrap=0 --base2lsbf', @\")\<CR>\<ESC>"
    \            , 'hex':       "gvc\<C-R>=system('xxd -cols 0 -plain \| tr -d \"\\n\"', @\")\<CR>\<ESC>"
    \            }
    call fzf#run({ 'source': sort(keys(encode))
    \            , 'sinklist': ({ m -> execute('normal! ' . encode[m[0]]) })
    \            , 'window': { 'width': 0.3, 'height': 0.3 }
    \            })
endfunction

function! encode#decode_sel() abort
    let encode = { 'base64':    "gvc\<C-R>=system('basenc --wrap=0 --decode --base64', @\")\<CR>\<ESC>"
    \            , 'base64url': "gvc\<C-R>=system('basenc --wrap=0 --decode --base64url', @\")\<CR>\<ESC>"
    \            , 'base32':    "gvc\<C-R>=system('basenc --wrap=0 --decode --base32', @\")\<CR>\<ESC>"
    \            , 'base32hex': "gvc\<C-R>=system('basenc --wrap=0 --decode --base32hex', @\")\<CR>\<ESC>"
    \            , 'base16':    "gvc\<C-R>=system('basenc --wrap=0 --decode --base16', @\")\<CR>\<ESC>"
    \            , 'base2msbf': "gvc\<C-R>=system('basenc --wrap=0 --decode --base2msbf', @\")\<CR>\<ESC>"
    \            , 'base2lsbf': "gvc\<C-R>=system('basenc --wrap=0 --decode --base2lsbf', @\")\<CR>\<ESC>"
    \            , 'hex':       "gvc\<C-R>=system('xxd -cols 0 -plain -revert \| tr -d \"\\n\"', @\")\<CR>\<ESC>"
    \            }
    call fzf#run({ 'source': sort(keys(encode))
    \            , 'sinklist': ({ m -> execute('normal! ' . encode[m[0]]) })
    \            , 'window': { 'width': 0.3, 'height': 0.3 }
    \            })
endfunction

function! encode#hash_sel(e) abort
    let cksum = executable('gcksum') ? 'gcksum' : 'cksum'
    let encode = { 'base64': "basenc --wrap 0 --base64"
    \            , 'hex' :   "xxd -cols 0 -plain \| tr -d \"\\n\""
    \            }
    let hash = { 'blake2b': "gvc\<C-R>=system('" . cksum . " --raw --algorithm blake2b \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'md4':     "gvc\<C-R>=system('openssl md4 -binary -provider legacy \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'md5':     "gvc\<C-R>=system('" . cksum . " --raw --algorithm md5 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'sha1':    "gvc\<C-R>=system('" . cksum . " --raw --algorithm sha1 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'sha224':  "gvc\<C-R>=system('" . cksum . " --raw --algorithm sha224 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'sha256':  "gvc\<C-R>=system('" . cksum . " --raw --algorithm sha256 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'sha384':  "gvc\<C-R>=system('" . cksum . " --raw --algorithm sha384 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          , 'sha512':  "gvc\<C-R>=system('" . cksum . " --raw --algorithm sha512 \| " . encode[a:e] . "', @\")\<CR>\<ESC>"
    \          }
    call fzf#run({ 'source': sort(keys(hash))
    \            , 'sinklist': ({ m -> execute('normal! ' . hash[m[0]]) })
    \            , 'window': { 'width': 0.3, 'height': 0.3 }
    \            })
endfunction
