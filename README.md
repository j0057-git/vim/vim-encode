# vim-encode

Plugin for encoding/decoding some text to/from Base64, hex, binary, etc. Also
supports some hashes.

Depends on FZF, and uses `basenc`, `gcksum`/`cksum`, `xxd` and `openssl-md4`.

## Using

Install this plugin in whatever way you do, and then set up visual mode
mappings:

    vmap <silent> <LEADER>E  <Plug>(encode#Encode)
    vmap <silent> <LEADER>D  <Plug>(encode#Decode)
    vmap <silent> <LEADER>HH <Plug>(encode#HashHex)
    vmap <silent> <LEADER>HB <Plug>(encode#HashBase64)

Enjoy!
